$(document).ready(function () {

    $('form.form_seller').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email:true
            },
            address: {
                required: true
            },
            phone_number: {
                required: true
            },
            type_card: {
                required: true
            },
            number_card: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Name is required',
            },
            email: {
                required: 'Email is required',
                email:'Email is wrong'
            },
            address: {
                required: 'Address is required',
            },
            phone_number: {
                required: 'Phone number is required',
            },
            type_card: {
                required: 'Type card is required',
            },
            number_card: {
                required: 'number card is requried'
            }
        }
    });
    $('button.edit').click(function () {
        $('#submit_button').text('Update');
        $('#submit_button').addClass('update');
        $('#submit_button').removeClass('insert');
        $("#modal_seller").modal("show");
        var form = $('form.form_seller');
        form.attr('data-id',$(this).attr('data-id'));
        var table_body = $('table>tbody.body-seller');
        $.ajax({
            url:table_body.attr('data-show')+'/'+$(this).attr('data-id'),
            dataType:'JSON',
            method:'get',
            type:'get',
            success:function(response){
                if(response.status=='ok'){
                    putFormSeller(response.data);
                }
            }
        })
    });

    $('button.delete').click(function(){
        var table_body = $('table>tbody.body-seller');
        var id = $(this).attr('data-id');
        $.confirm({
            title: 'Delete!',
            content: 'Delete this customer!',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url:table_body.attr('data-delete')+'/'+id,
                        method:'delete',
                        type:'delete',
                        headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                        success:function (response) {
                            if(response.status=='ok'){
                                alertSeller('Vendedor guardado',true);
                            }else{
                                alertSeller('Hubo un error',false);
                            }
                        }
                    });
                },
                cancel: function () {
                    // $.alert('Canceled!');
                }
            }
        });
    });
    var alertSeller = function(message,bool_reload){
        $.alert({
            title: 'Alert!',
            content: message,
            buttons: {
                confirm: {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function () {
                        if(bool_reload){
                            window.location.reload();
                        }
                    }
                },
            }
        });
    }
    $('button.seller').click(function (e) {
        e.preventDefault();
        var form = $('form.form_seller');
        if($(this).hasClass('insert')) {
            if (form.valid()) {
                $.ajax({
                    url: form.attr('action'),
                    method: form.attr('method'),
                    type: form.attr('method'),
                    dataType: 'json',
                    async: false,
                    data: form.serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status='ok'){
                            alertSeller('Vendedor guardado',true);
                        }else{
                            alertSeller('Hubo un error',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }else{
            if (form.valid()) {
                $.ajax({
                    url: form.attr('data-update')+'/'+form.attr('data-id'),
                    method: 'put',
                    type: 'put',
                    dataType: 'json',
                    async: false,
                    data: form.serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status='ok'){
                            alertSeller('Vendedor guardado',true);
                        }else{
                            alertSeller('Hubo un error',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }
    });

    var putFormSeller = function (seller) {
        $("#name").val(seller.name);
        $("#email").val(seller.email);
        $("#address").val(seller.address);
        $("#type_card").val(seller.type_card);
        $("#number_card").val(seller.number_card);
        $("#phone_number").val(seller.phone_number);
        $('#type_card > option').each(function (index,value) {
            if($(value).val()==seller.type_card){
                $(value).prop('selected', true);
            }
        })
  };
});