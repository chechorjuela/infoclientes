$(document).ready(function () {

   //tableCustomer();

    $('form.form_customer').validate({
        rules: {
            name: {
                required: true,
            },
            nit: {
                required: true,
                number:true
            },
            address: {
                required: true
            },
            quota: {
                required: true,
                number:true
            },
            balance_quota: {
                required: true,
                number:true
            },
            percentage_visits: {
                required: true
            },
            country: {
                required: true
            },
            departament: {
                required: true
            },
            city_id: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Campo obligatorio',
            },
            nit: {
                required: 'Campo obligatorio',
                number:"Solamente se permite numeros"
            },
            address: {
                required: 'Direccion obligatorio',
            },
            quota: {
                required: 'Cuota obligatoria',
                number:"Solamente se permite numeros"
            },
            balance_quota: {
                required: 'Balance quota is required',
                number:"Balance se permite numeros"
            },
            percentage_visits: {
                required: 'Percentage visits is requried'
            },
            country: {
                required: 'Country is required'
            },
            departament: {
                required: 'Departament is required'
            },
            city_id: {
                required: 'City is required'
            }
        }
    });

    $('form.form_customer_update').validate({
        rules: {
            name: {
                required: true,
            },
            address: {
                required: true
            },
            quota: {
                required: true
            },
            balance_quota: {
                required: true
            },
            percentage_visits: {
                required: true
            },
            country: {
                required: true
            },
            departament: {
                required: true
            },
            city_id: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Name is required',
            },
            address: {
                required: 'Address is required',
            },
            quota: {
                required: 'Quota is required',
            },
            balance_quota: {
                required: 'Balance quota is required',
            },
            percentage_visits: {
                required: 'Percentage visits is requried'
            },
            country: {
                required: 'Country is required'
            },
            departament: {
                required: 'Departament is required'
            },
            city_id: {
                required: 'City is required'
            }
        }
    });

    $('#modal_customer').on('shown.bs.modal', function() {

    });
    $('#modal_customer').on('hidden.bs.modal', function() {
        $(".nit_field").html(html_nit);
        $('#submit_button').text('Guadar');
        $('#submit_button').removeClass('update');
        $('#submit_button').addClass('insert');
        $('form.form_customer input').val("");
        $('form.form_customer select#country').val("");
        $('form.form_customer select#departament,form.form_customer select#City').find('option').remove().end()
            .append('<option value="">Selected</option>');
        $('[name="percentage_visits"]').slider('setValue',0);
        $('span.percentageValue').text('0%');

    });

    $('form.form_customer')
        .find('[name="percentage_visits"]')
        .each(function() {
            $(this)
            // Create a slider
                .slider({
                    min: 0,
                    max: 100,
                    step: 1,
                    tooltip: 'hide'
                })
                // Triggered after dragging the slider
                .on('slide', function(e) {
                    var $field = $(e.target);
                    $field
                        .closest('.form-group')
                        .find('.percentageValue')
                        .html($field.slider('getValue') + '%');
                });
        })
        .end();




    $('button.delete').click(function(){
        var table_body = $('table>tbody.body-customer');
        var id = $(this).attr('data-id');
        $.confirm({
            title: 'Delete!',
            content: 'Delete this customer!',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url:table_body.attr('data-delete')+'/'+id,
                        method:'delete',
                        type:'delete',
                        headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                        success:function (response) {
                            if(response.status=='ok'){
                                alertCustomer("Cliente eliminado",true);
                            }else{
                                alertCustomer("Hubo un error.",false)
                            }
                        }
                    });
                },
                cancel: function () {
                   // $.alert('Canceled!');
                }
            }
        });
    });
    var html_nit = $(".nit_field").html();
    $('button.edit').click(function () {
        $('#submit_button').text('Actualizar');
        $('#submit_button').addClass('update');
        $('#submit_button').removeClass('insert');
        $(".nit_field").html("");
        $("#modal_customer").modal("show");
        var form = $('form.form_customer');
        form.attr('data-id',$(this).attr('data-id'));
        var table_body = $('table>tbody.body-customer');
        $.ajax({
            url:table_body.attr('data-show')+'/'+$(this).attr('data-id'),
            dataType:'JSON',
            method:'get',
            type:'get',
            success:function(response){
                if(response.status=='ok'){
                    putFormCustomer(response.data);
                }

            }
        })
    });

    $('button.customer').click(function (e) {
        e.preventDefault();
        var form = $('form.form_customer');
        if($(this).hasClass('insert')) {
            if (form.valid()) {
                $.ajax({
                    url: form.attr('action'),
                    method: form.attr('method'),
                    type: form.attr('method'),
                    dataType: 'json',
                    async: false,
                    data: form.serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status=='ok'){
                            alertCustomer('Pagina se va actualizar!',true);
                            //window.location.reload();
                        }else{
                            alertCustomer('Pagina se va actualizar!',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }else{
            if (form.valid()) {
                $.ajax({
                    url: form.attr('data-update')+'/'+form.attr('data-id'),
                    method: 'put',
                    type: 'put',
                    dataType: 'json',
                    async: false,
                    data: form.serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status=='ok'){
                            alertCustomer('Pagina se va actualizar!',true);
                           // window.location.reload();
                        }else{
                            alertCustomer('Pagina se va actualizar!',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }
    });
    var alertCustomer = function(message,bool_reload){
        $.alert({
            title: 'Alert!',
            content: message,
            buttons: {
                confirm: {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function () {
                        if(bool_reload){
                            window.location.reload();
                        }
                    }
                },
            }
        });
    }
    $("#country").change(function (e) {
        var country = $(this);
        $('form.form_customer select#departament,form.form_customer select#City').find('option').remove().end()
            .append('<option value="">Selected</option>');
        if(country.val()){
            $.ajax({
                url: country.attr('data-url') + '/' + country.val(),
                method: 'get',
                type: 'get',
                dataType: 'JSON',
                success: function (response) {
                    if (response.status=='ok') {
                        var departament = $("#departament");
                        dataPut(departament, response.data)

                    }
                }
            })
        }

    });
    $("#departament").change(function () {
        var departament = $(this);
        $('form.form_customer select#City').find('option').remove().end()
            .append('<option value="">Selected</option>');
        if(departament.val()){
            $.ajax({
                url: departament.attr('data-url') + '/' + departament.val(),
                method: 'get',
                type: 'get',
                dataType: 'JSON',
                success: function (response) {
                    if (response.status) {
                        var city = $("#city");
                        dataPut(city, response.data)
                    }
                }
            });
        }

    });
    var dataPut = function (element, data) {
        element.prop('disabled', false);

        $.each(data, function (index, value) {

            element.append('<option value="' + value.id + '">' + value.name + '</option>');
        })
    };

    var putFormCustomer = function (customer) {
        $("#name").val(customer.name);
        $("#nit").val(customer.nit);
        $("#address").val(customer.address);
        $("#phone").val(customer.phone);
        $("#quota").val(customer.quota);
        $('[name="percentage_visits"]').slider('setValue',customer.percentage_visits);
        $('span.percentageValue').text(customer.percentage_visits+'%');
        $("#balance_quota").val(customer.balance_quota);
        $("#percentage_visits").val(customer.percentage_visits);
        locationSelect($('#country'),customer.country.id,null);
        locationSelect($("#departament"),customer.departament.id,customer.country.id);
        locationSelect($("#city"),customer.city_id,customer.departament.id);
    };

    var locationSelect = function(select,selected,parent){
        console.info(selected);
        var url = select.attr('data-get');
        if(parent){
            $.ajax({
                url:url,
                method: 'get',
                type: 'get',
                dataType: 'JSON',
                success:function (response) {
                    select.prop('disabled', false);

                    $.each(response.data, function (index, value) {
                        var selected_item='';
                        if(selected==index){

                            selected_item='selected';
                        }
                        select.append('<option '+selected_item+' value="' + index + '">' + value + '</option>');
                    })
                }
            });
        }else{
            $('#country > option').each(function (index,value) {
                if($(value).val()==selected){
                    $(value).prop('selected', true);
                }
            })
        }

        //console.info(select.attr('data-url'));
    };
    function tableCustomer() {
        var table_body = $('table>tbody.body-customer');
        $.ajax({
            url:table_body.attr('data-url'),
            method:'get',
            type:'get',
            async:false,
            success:function(response){
                if(response.status=='ok'){
                   var table = registerTable(response.data);
                    table_body.html(table);
                }
            }
        });
    }

    function registerTable(data) {
        var tableBody;
        $.each(data.data,function (index,value) {
            tableBody+='<tr>' +
                '<td>'+value.id+'</td>'+
                '<td>'+value.name+'</td>'+
                '<td>'+value.nit+'</td>'+
                '<td>'+value.address+'</td>'+
                '<td>'+value.phone+'</td>'+
                '<td>'+value.quota+'</td>'+
                '<td>'+value.balance_quota+'</td>'+
                '<td>'+value.percentage_visits+'</td>'+
                '<td>'+value.city.name+','+value.country.name+'</td>'+
                '<td><button data-id="'+value.id+'" class="btn btn-primary edit">Edit</button></td>'+
                '<td><button data-id="'+value.id+'" class="btn btn-danger delete">Delete</button></td>'+
                '</tr>';
        });

        return tableBody;
    }
});