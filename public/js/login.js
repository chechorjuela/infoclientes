$(document).ready(function () {
    $("form.form_login").validate({
        rules:{
            email:{
                required:true,
                email:true,
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                required:"Email is required",
                email:"Wrong email",
            },
            password:{
                required:"Password is required"
            }
        }
    });

    $('form.form_login').submit(function (e) {
        e.preventDefault();
        if($(this).valid()){
            $.ajax({
                url : $(this).attr('action'),
                method : $(this).attr('method'),
                type:$(this).attr('method'),
                dataType:'json',
                async:false,
                data : $(this).serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                success : function (response) {
                    $('.error-login').html("");
                    if(response.status=="success"){
                        window.location.href = response.url;
                    }else{
                        $(".error-login").append("<p>"+response.message+"</p>").fadeIn();
                        $.each(function (index,value) {
                            $("input#index").append('<div class="error">'+value+'</div>');
                        });
                    }

                },
                error:function () {

                }
            });
        }

    })
});