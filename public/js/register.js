$(document).ready(function () {
    $('form.form_register').validate({
        rules: {
            name: {
                required: true,

            },
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true
            },
            rpt_password: {
                equalTo: "#password"
            }
        },
        messages: {
            name: {
                required: 'Name is required',
            },
            email: {
                required: 'Email is required',
                email: 'Format incorrect',
            },
            password: {
                required: 'Password is required'
            },
            rpt_password: {
                equalTo: 'Repeat password equal password'
            }
        }
    });
    $('form.form_register').submit(function (e) {
        e.preventDefault();

          if($(this).valid()){
              $.ajax({
                  url : $(this).attr('action'),
                  method : $(this).attr('method'),
                  type:$(this).attr('method'),
                  dataType:'json',
                  async:false,
                  data : $(this).serialize(),
                  headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                  success : function (response) {
                      $('.error-register').html("");
                      if(response.status=='ok'){
                          $('.error-register').fadeIn().append('<p>Cuenta creada ya puedes iniciar sección.</p>');
                      }else{
                          console.info('mostrando');
                          $('.error-register').fadeIn().append('<p>Ups ha pasado algo</p>');
                      }
                  },
                  error:function () {

                  }
              });
          }

    })
});