$(document).ready(function () {
    $("#date").datepicker({
        format: 'yyyy-mm-dd',
    });
    $('form.form_visit').validate({
        rules:{
            customer:{
                required:true
            },
            vendedor:{
                required:true
            },
            date:{
                required:true,
            },
            value_neto:{
                required:true
            },
            value_visit:{
                required:true
            },
            observation_visit:{
                required:true
            }
        }
    });

    $("#value_neto").focusout(function () {

    });

    $('#vendedor').autocomplete({
        serviceUrl: $("#vendedor").attr('data-url'),
        noCache:false,
        minChars: 1,
        maxHeight: 1520,
        onSelect: function (response) {

            $('#vendedor').attr('data-id',response.id);
        },
        autoSelectFirst: true,

    });

    $("#customer").autocomplete({
        serviceUrl: $("#customer").attr('data-url'),
        noCache:false,
        minChars: 1,
        maxHeight: 1520,
        onSelect: function (response) {
            $('#customer').attr('data-id',response.id);
        },
        autoSelectFirst: true,

    });

    $("button.visit").click(function (e) {
        e.preventDefault();
        $("#modal_visit").modal('show');
        var table_body = $('table>tbody.body-customer');
        $.ajax({
            url:table_body.attr('data-show')+'/'+$(this).attr('data-id'),
            dataType:'JSON',
            method:'get',
            type:'get',
            success:function(response){
                if(response.status=='ok'){
                    $("#customer").val(response.data.name).attr('data-id',response.data.id);
                }
            }
        })
    });

    $('button.visits').click(function (e) {
        e.preventDefault();
        var form = $('form.form_visit');
        var data = form.serializeArray();
        data.push(
            {'name':'customer_id','value':$("#customer").attr('data-id')},
            {'name':'seller_id','value':$("#vendedor").attr('data-id')}
            );
        if($(this).hasClass('insert')) {
            if (form.valid()) {
                $.ajax({
                    url: form.attr('action'),
                    method: form.attr('method'),
                    type: form.attr('method'),
                    dataType: 'json',
                    async: false,
                    data: data,
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status='ok'){
                            alertVisit('Se ha guardado la visita',true);
                        }else{
                            alertVisit('Hubo un error',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }else{
            if (form.valid()) {
                $.ajax({
                    url: form.attr('data-update')+'/'+form.attr('data-id'),
                    method: 'put',
                    type: 'put',
                    dataType: 'json',
                    async: false,
                    data: form.serialize(),
                    headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')},
                    success: function (response) {
                        if(response.status='ok'){
                            alertVisit('Se ha guardado la visita',true);
                        }else{
                            alertVisit('Hubo un error',false);
                        }
                    },
                    error: function () {

                    }
                });
            }
        }
    });
    var alertVisit = function(message,bool_reload){
        $.alert({
            title: 'Alert!',
            content: message,
            buttons: {
                confirm: {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function () {
                        if(bool_reload){
                            window.location.reload();
                        }
                    }
                },
            }
        });
    }
});