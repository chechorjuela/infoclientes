<?php
use App\Http\Middleware\CheckUser;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(CheckUser::class);

Route::get('login',[
    'as'=>'home.login',
    'uses'=>'HomeController@login'
]);


Route::get('register',[
    'as'=>'home.register',
    'uses'=>'HomeController@register'
]);

Route::post('singin',[
    'as'=>'home.singin',
    'uses'=>'HomeController@singin'
]);

Route::post('singup',[
    'as'=>'home.singup',
    'uses'=>'HomeController@singup'
]);

Route::get('logout',[
    'as'=>'home.logout',
    'uses'=>'Auth\LoginController@logout'
]);

Route::get('admin/customers',[
    'as'=>'admin.customers',
    'uses'=>'AdminController@customers'
]);

Route::get('admin/visits',[
    'as'=>'admin.visits',
    'uses'=>'AdminController@visits'
]);

Route::get('admin/reports/cities',[
    'as'=>'admin.reports.cities',
    'uses'=>'AdminController@reportsCities'
]);

Route::get('admin/reports/customers/{id?}',[
    'as'=>'admin.reports.customers',
    'uses'=>'AdminController@reportsCustomers'
]);

Route::get('admin/sellers',[
    'as'=>'admin.sellers',
    'uses'=>'AdminController@sellers'
]);
Route::get('cities_departaments/{id?}',[
    'as'=>'cities.departament',
    'uses'=>'CitiesController@citiesDepartamentId'
]);
Route::get('departaments_city/{id?}',[
    'as'=>'departaments.city',
    'uses'=>'DepartamentsController@departamentByCity'
]);
Route::resource('customers','CustomersController');
Route::resource('countries','CountriesController');
Route::resource('departaments','DepartamentsController');
Route::resource('city','CitiesController');
Route::resource('visits','VisitsController');
Route::resource('sellers','SellersController');