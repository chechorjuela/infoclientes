<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name','address','phone','quota','balance_quota','percentage_visits','city_id'];

    protected $hidden = [
        'nit'
    ];

    public function city(){

        return $this->belongsTo('App\City');
    }

    public function visits(){
        return $this->hasMany('App\Visit');
    }

    public function HistorySaldo(){
        return $this->hasMany('App\CustomerHistory');
    }

    public static function preparedCustomer($customer){
        $customer->city = $customer->city;
        $customer->departament = $customer->city->departament;
        $customer->country = $customer->city->departament->country;
        return $customer;
    }
}