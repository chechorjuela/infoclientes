<?php

namespace App\Listeners;

use App\Customer;
use App\CustomerHistory;
use App\Events\UpdatePorcentuageBalanceCustomer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateBalanceCustomer
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  UpdatePorcentuageBalanceCustomer  $event
     * @return void
     */
    public function handle(UpdatePorcentuageBalanceCustomer $event)
    {
        $this->registerHistoryBalance($event->visit);
    }

    private function registerHistoryBalance($visit){
        $totale_quota = $visit->customer->balance_quota - ($visit->value_visits);
        $history = new CustomerHistory();
        $history->visit_id = $visit->id;
        $history->customer_id = $visit->customer->id;
        $history->value = $totale_quota;
        $history->save();

        $this->updateCustomerQuota($visit->customer,$totale_quota);
    }

    private function updateCustomerQuota($customer,$totale){

        $customer->balance_quota = $totale;
        $customer->save();
    }
}
