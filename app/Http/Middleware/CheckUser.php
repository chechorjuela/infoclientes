<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $auth = !Auth::guest();
        $url =  str_replace('/','',$request->getRequestUri());
        if($auth):
            if($url!='register' && $url!='login' && $url!=''):
                return $next($request);
            else:
                return redirect()->route('admin.customers');
            endif;
        else:

            if($url=='register' || $url=='login'):
                return $next($request);
            else:
                return redirect()->route('home.login');
            endif;
        endif;

    }
}
