<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Visit;
use Illuminate\Http\Request;

class VisitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $customer = Customer::find($request->customer_id);
        $visit = new Visit();
        $visit_save = $visit->create([
            'seller_id' => $request->seller_id,
            'customer_id' => $request->customer_id,
            'value_neto' => $request->value_neto,
            'date'=>$request->date,
            'value_visits' => $request->value_neto * $customer->percentage_visits,
            'observation_visit' => $request->observation_visit,
        ]);
        if ($visit_save):
            $response['status'] = 'ok';
            $response['data'] = $visit_save;
        endif;

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (is_numeric($id)):

        elseif (is_string($id)):
            dd($id);
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
