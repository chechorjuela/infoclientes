<?php

namespace App\Http\Controllers;

use App\City;
use App\Customer;
use App\CustomerHistory;
use App\Seller;
use App\Visit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Country;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.user');
    }

    public function customers(){
        $customers = Customer::paginate(10);
        $countries = Country::pluck("name", "id")->all();
        return view('admin.customers')->with(['customers'=>$customers,'countries'=>$countries]);
    }


    public function visits(){
        $visits = Visit::paginate(10);
        return view('admin.visits',['visits'=>$visits]);
    }

    public function reports(){

        return view('admin.reports');
    }

    public function reportsCities(){

        $lava = new Lavacharts();

        $votes  = $lava->DataTable();

        $votes->addStringColumn('Customers')
            ->addNumberColumn('Visits');
        $cities = City::select(DB::raw('cities.*'),DB::raw('(SELECT COUNT(*) '.
            'FROM visits vi '.
            'INNER JOIN customers ON customers.id = vi.customer_id '.
            'WHERE customers.city_id = cities.id) as totale'))
            ->leftJoin('customers','cities.id', '=' ,'customers.city_id')
            ->groupBy('cities.id')->get();

        foreach ($cities as $city):
            $votes->addRow([$city->name,$city->totale]);
        endforeach;

        $lava->BarChart('Votes', $votes, [
            'title' => 'Visitas clientes por cuidades',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14]
        ]);

        return view('admin.reports.cities',['lava'=>$lava]);
    }

    public function reportsCustomers($id){

        $lava = new Lavacharts; // See note below for Laravel

        $movements = $lava->DataTable();

        $movements->addStringColumn('String')
            ->addNumberColumn('Movimientos Saldo');


        $customer_histories = CustomerHistory::where('customer_id',$id)->get();

        foreach ($customer_histories as $customer_history):

            $movements->addRow([$customer_history->created_at->format('Y-m-d'),$customer_history->value]);
        endforeach;


        $lava->LineChart('Temps', $movements, [
            'title' => 'Movimiento de cupos'
        ]);
        return view('admin.reports.customer',['lava'=>$lava]);
    }

    public function sellers(){
        $sellers = Seller::paginate(10);
        return view('admin.sellers',['sellers'=>$sellers]);
    }

}
