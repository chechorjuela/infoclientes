<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    private function _preparedCustomer($customers){
        foreach ($customers as &$customer):
            $customer = Customer::preparedCustomer($customer);
        endforeach;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = ['status' => 'fail', 'suggestions' => []];
        $customers = Customer::where('name', 'like', "%{$request['query']}%")->get();
        $suggestions = [];
        if ($customers):
            foreach ($customers as $customer):
                $suggestions[] = ['id' => $customer->id, 'value' => $customer->name];
            endforeach;

        else:
            $suggestions[] = ['value' => "No existe Pagador", 'data' => "null"];
        endif;
        $response['suggestions'] = $suggestions;
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response=['status'=>'fail','data'=>[]];
        $customer = new Customer();
        $customer_save = $customer->create([
            'name'=> $request->name,
            'nit'=> bcrypt($request->nit),
            'address'=> $request->address,
            'phone'=> $request->phone,
            'quota'=> $request->quota,
            'balance_quota'=> $request->percentage_visits/100,
            'percentage_visits'=> $request->percentage_visits,
            'city_id'=> $request->city_id]);

        if($customer_save):
            $response['status']='ok';
            $response['data']=$customer_save;
        endif;

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response=['status'=>'fail','data'=>[]];
        $customer = Customer::find($id);
        if($customer){
            $response['status']='ok';
            $this->_preparedCustomer([$customer]);
            $response['data']=$customer;
        }
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response=['status'=>'fail','data'=>[]];
        $customer = Customer::find($id);

        $customer->fill($request->all());
        $customer_save = $customer->save();
        if($customer_save):
            $response['status']='ok';
            $this->_preparedCustomer([$customer]);
            $response['data']=$customer;
        endif;
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response=['status'=>'fail','data'=>[]];
        $customer = Customer::find($id);
        $delete_customer = $customer->delete();
        if($delete_customer):
            $response['status']='ok';
            $response['data']='Customer is deleted';
        endif;
        return response()->json($response);
    }
}
