<?php

namespace App\Http\Controllers;

use App\Seller;
use Illuminate\Http\Request;

class SellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = ['status' => 'fail', 'suggestions' => []];
        $sellers = Seller::where('name', 'like', "%{$request['query']}%")->get();
        $suggestions = [];
        if ($sellers):
            foreach ($sellers as $seller):
                $suggestions[] = ['id' => $seller->id, 'value' => $seller->name];
            endforeach;

        else:
            $suggestions[] = ['value' => "No existe Pagador", 'data' => "null"];
        endif;
        $response['suggestions'] = $suggestions;
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['status' => 'fail', 'data' => []];
        $seller = new Seller();
        $seller_save = $seller->create([
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'type_card' => $request->type_card,
            'number_card' => $request->number_card,
            'phone_number' => $request->phone_number,
        ]);

        if ($seller_save):
            $response['status'] = 'ok';
            $response['data'] = $seller_save;
        endif;

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $seller = Seller::find($id);
        if ($seller) {
            $response['status'] = 'ok';
            //$this->_preparedCustomer([$customer]);
            $response['data'] = $seller;
        }
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $seller = Seller::find($id);
        $seller->fill($request->all());
        $seller_save = $seller->save();
        if ($seller_save):
            $response['status'] = 'ok';
            $response['data'] = $seller_save;
        endif;
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = ['status' => 'fail', 'data' => []];
        $seller = Seller::find($id);
        $delete_seller = $seller->delete();
        if ($delete_seller):
            $response['status'] = 'ok';
            $response['data'] = 'Customer is deleted';
        endif;
        return response()->json($response);
    }
}
