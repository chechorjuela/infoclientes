<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Exception;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('check.user')->except(['singup','singin']);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function singup(Request $request)
    {
        $response=['status'=>'fail','data'=>[]];
        if ($request->ajax()):
            try{
                $user = User::create(["name" => $request->name, 'email' => $request->email, "password" => bcrypt($request->password)]);
                $user_save = $user->save();
                if($user_save):
                    $response['status'] = 'ok';
                    $response['data']= $user_save;
                endif;
            }catch (Exception $e){
                $response['data']='Error in save';
            }
        endif;
        return response()->json($response);
    }

    public function singin(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (empty($email)) :
            $response['message'] = 'Email esta vacio.';
        elseif (empty($password)) :
            $response['message'] = 'Contraseña esta vacio.';
        elseif (!empty($email) && !empty($password)) :
            $user = User::where('email', '=', $email)->get()->first();
            if ($user):
                if ($user->status == 1) :
                    if (Auth::attempt(['email' => $email, 'password' => $password], true, true)):
                        $response['status'] = 'success';
                        $response['id'] = $user->id;
                        $response['url'] = route('admin.customers');
                    else:
                        $response['status'] = 'fail';
                        $response['message'] = 'Contraseña incorrecta.';
                    endif;
                else:
                    $response['status'] = 'fail';
                    $response['message'] = 'Usuario no ha validado email.';
                endif;
            else:
                $response['status'] = 'fail';
                $response['message'] = 'Usuario no existe.';
            endif;

        else:
            $response['status'] = 'fail';
            $response['message'] = 'Email or Password is wrong';
        endif;

        return response()->json($response);
    }

    public function logout(){

    }
}
