<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = ['name','email','address','phone_number','type_card','number_card'];

    public function visits(){
        return $this->hasMany('App\Visit');
    }

}