<?php

namespace App;

use App\Events\UpdatePorcentuageBalanceCustomer;
use App\Listeners\UpdateBalanceCustomer;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = ['seller_id','customer_id','value_neto','value_visits','date','observation_visit'];
    protected $events = [
        'saved' => UpdatePorcentuageBalanceCustomer::class
    ];
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function seller(){
        return $this->belongsTo('App\Seller');
    }
}
