<?php

namespace App\Events;

use App\Customer;
use App\Visit;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdatePorcentuageBalanceCustomer
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $visit;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Visit $vist)
    {
        $this->visit = $vist;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
