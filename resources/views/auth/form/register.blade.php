<div class="center-align center-block">
    <div class="container">
        <div class="row justify-content-center">
            <div class='col-6'>
        {!! Form::open(['url'=>route('home.singup'),'method'=>'post','class'=>'form_register']) !!}

            <div class='form-group'>
                <label for='name'>Enter your name</label>
                <input class='form-control' type='text' name='name' id='name'/>

            </div>

            <div class='form-group'>
                <label for='email'>Enter your email</label>
                <input class='form-control' type='text' name='email' id='email'/>

            </div>
            <div class='form-group'>
                <label for='password'>Enter your password</label>
                <input class='form-control' type='password' name='password' id='password'/>

            </div>

            <div class='form-group'>
                <label for='rpt_password'>Enter your repeat password</label>
                <input class='form-control' type='password' name='rpt_password' id='rpt_password'/>

            </div>
            <div class='form-group'>
                <button type='submit' name='btn_register' class='btn btn-primary'>Register
                </button>
            </div>
                <div class="alert-danger error-register" style="display: none"></div>
            {!! Form::close() !!}
            </div>
    </div>
</div>
@section('scripts')
    {!! Html::script('js/register.js') !!}
@endsection