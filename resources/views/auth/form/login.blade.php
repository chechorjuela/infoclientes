<div class="center-align center-block">
    <div class="container">
        <div class="row  justify-content-center">
            <div class="col-6">
                {!! Form::open(['url'=>route('home.singin'),'method'=>'post','class'=>'form_login']) !!}

                <div class='form-group'>
                    <label for='email'>Enter your email</label>
                    <input class='form-control' type='email' name='email' id='email'/>

                </div>

                <div class='form-group'>
                    <label for='password'>Enter your password</label>
                    <input class='form-control' type='password' name='password' id='password'/>
                </div>

                <div class='form-group'>
                    <label style='float: right;'>
                        <a class='pink-text' href='#!'><b>Forgot Password?</b></a>
                    </label>
                </div>

                <div class='form-group'>
                    <button type='submit' name='btn_login' class='btn btn-primary'>Login</button>
                </div>
                <div class="alert-danger error-login"></div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
    <a href="#!">Create account</a>
</div>
@section('scripts')
    {!! Html::script('js/login.js') !!}
@endsection