@extends('layout.layout')
@section('content')
    <div class="">
        @include('auth.form.login')
    </div>
    <div class="" style="display: none">
        @include('auth.form.register')
    </div>
@endsection
