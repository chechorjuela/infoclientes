<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand title-top" href="#">Infoweb</a>
    @if(Auth::guest())
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav  pull-xs-right my-2 my-lg-0 mr-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{!! route('home.login') !!}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('home.register') !!}">Registrar</a>
                </li>
            </ul>
        </div>

    @else
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class=" navbar-nav mr-auto pull-right">
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('admin.customers') !!}">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('admin.visits') !!}">Visitas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('admin.sellers') !!}">Vendedores</a>
                </li>
                <li class="nav-item dropdown">

                    <a class="dropdown-toggle nav-link" id="dropdownMenuButton" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Reportes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{!! route('admin.reports.cities') !!}">Cuidades</a>
                        <a class="dropdown-item" href="{!! route('admin.reports.customers') !!}">Cliente</a>
                    </div>
                </li>
                <li>
                     <span class="navbar-text hidden-lg-up">
                        <a href="{!! route('home.logout') !!}">Logout</a>
                    </span>
                </li>
            </ul>

        </div>
        <span class="navbar-text hidden-md-down">
            <a href="{!! route('home.logout') !!}">Logout</a>
        </span>

    @endif
</nav>
