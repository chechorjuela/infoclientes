<html lang="en">
<head>
    <title>Topfived</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!!csrf_token()!!}"/>
    @include('layout.headlinks.headlinks')

</head>
<body>
<header>
    @include('layout.header.header')
</header>
<div class="container">
    @yield('content')
</div>

@include('layout.footer.footer')
@include('layout.headlinks.scriptslinks')
@yield('scripts')
</body>
</html>