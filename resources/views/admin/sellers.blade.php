@extends('layout.layout')
@section('content')
    <h1>Vendedores</h1>
    <div class="row">
        <div class="col-12 text-right">
            <button class="btn pull-right btn-primary"  data-toggle="modal" data-target="#modal_seller"><i class="material-icons">Agregar</i></button>
        </div>
    </div>

    @if(count($sellers)>0)
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Telefono</th>
                <th>Tipo de documento</th>
                <th>Numero de tarjeta</th>
                <th class="text-center" colspan="2">Action</th>
            </tr>
            </thead>
            <tbody class="body-seller" data-url="{!! route('sellers.index') !!}" data-delete="{!! route('sellers.destroy',null) !!}"
                   data-show="{!! route('sellers.show',null) !!}">
            @foreach($sellers as $seller)
                <tr>
                    <td>{!! $seller->id !!}</td>
                    <td>{!! $seller->name !!}</td>
                    <td>{!! $seller->address !!}</td>
                    <td>{!! $seller->phone_number !!}</td>
                    <td>{!! $seller->type_card !!}</td>
                    <td>{!! $seller->number_card !!}</td>
                    <td><button data-id="{!! $seller->id !!}" class="btn btn-primary edit">Editar</button></td>
                    <td><button data-id="{!! $seller->id !!}" class="btn btn-danger delete">Eliminar</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-12">
                {!!  $sellers->render(); !!}
            </div>
        </div>
    @else
        <h2>No existe vendedores.</h2>
    @endif
    @include('admin.modals.seller')
@endsection
@section('scripts')
    {!! Html::script('js/admin.js') !!}
    {!! Html::script('js/sellers.js') !!}
@endsection
