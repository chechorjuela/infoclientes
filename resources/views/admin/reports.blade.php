@extends('layout.layout')
@section('content')
    <h1>Reportes</h1>
    <div id="stocks-div"></div>
    {!! $lava->render('BarChart', 'Votes', 'stocks-div') !!}
@endsection
@section('scripts')
    {!! Html::script('js/admin.js') !!}
@endsection
