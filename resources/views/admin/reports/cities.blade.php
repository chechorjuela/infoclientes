@extends('layout.layout')
@section('content')
    <h1>Reportes por ciudades</h1>
    <div id="stocks-div" style="height: 520px"></div>
    {!! $lava->render('BarChart', 'Votes', 'stocks-div') !!}
@endsection
@section('scripts')
    {!! Html::script('js/admin.js') !!}
@endsection
