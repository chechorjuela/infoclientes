
@extends('layout.layout')
@section('content')
    <h1>Reporte por cliente saldo</h1>
    <div id="stocks-div" style="height: 520px"></div>
    {!! $lava->render('LineChart', 'Temps', 'stocks-div') !!}
@endsection
@section('scripts')
    {!! Html::script('js/admin.js') !!}
@endsection
