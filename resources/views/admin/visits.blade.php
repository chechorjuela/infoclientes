@extends('layout.layout')
@section('content')
    <h1>Visitas</h1>
    <div class="row">
        <div class="col-12 text-right">
            <button class="btn pull-right btn-primary"  data-toggle="modal" data-target="#modal_visit"><i class="material-icons">Agregar</i></button>
        </div>
    </div>

    @if(count($visits)>0)
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>id</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Valor neto</th>
                <th>Fecha</th>
                <th>Valor visita</th>
            </tr>
            </thead>
            <tbody class="body-customer" data-url="{!! route('customers.index') !!}" data-delete="{!! route('customers.destroy',null) !!}" data-show="{!! route('customers.show',null) !!}">
            @foreach($visits as $visit)
                <tr>
                    <td>{!! $visit->id !!}</td>
                    <td>{!! $visit->customer->name !!}</td>
                    <td>{!! $visit->seller->name !!}</td>
                    <td>{!! $visit->value_neto !!}</td>
                    <td>{!! $visit->date !!}</td>
                    <td>{!! $visit->value_visits !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-12">
                {!!  $visits->render(); !!}
            </div>
        </div>
    @else
        <h2>No existe visitas.</h2>
    @endif

    @include('admin.modals.visit')
@endsection
@section('scripts')
    {!! Html::script('js/visits.js') !!}
    {!! Html::script('js/admin.js') !!}
@endsection
