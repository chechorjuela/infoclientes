<!-- Modal -->
<div class="modal fade" id="modal_visit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Visitas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url'=>route('visits.store'),'method'=>'post','class'=>'form_visit',
                'data-update'=>route('visits.update',null),'data-store'=>route('visits.store'),'data-id'=>'']) !!}
                <div class="form-group">
                    <label for="customer">Cliente</label>
                    <input class="form-control" type="text" name="customer" id="customer" data-id="" data-url="{!! route('customers.index') !!}">
                </div>
                <div class='form-group'>
                    <label for='name'>Vendedor</label>
                    <input class='form-control' type='text' name='vendedor' id='vendedor' data-id="" data-url="{!! route('sellers.index') !!}"/>
                </div>
                <div class='form-group'>
                    <label for='name'>Fecha</label>
                    <input class='form-control' type='text' name='date' id='date'/>
                </div>
                <div class='form-group'>
                    <label for='name'>Valor neto</label>
                    <input class='form-control' type='text' name='value_neto' id='value_neto'/>
                </div>
                <div class='form-group'>
                    <label for='name'>Observacion visita</label>
                    <textarea cols="15" rows="10" name="observation_visit" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary visits insert" id="visit_submit">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
