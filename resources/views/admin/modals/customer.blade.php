<!-- Modal -->
<div class="modal fade" id="modal_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url'=>route('customers.store'),'method'=>'post','class'=>'form_customer','id'=>'form_customer',
                'data-update'=>route('customers.update',null),'data-store'=>route('customers.store'),'data-id'=>'']) !!}

                <div class='form-group'>
                    <label for='name'>Nombre</label>
                    <input class='form-control' type='text' name='name' id='name'/>
                </div>

                <div class='form-group nit_field'>
                    <label for='nit'>Nit</label>
                    <input class='form-control' type='text' name='nit' id='nit'/>

                </div>
                <div class='form-group'>
                    <label for='address'>Direccion</label>
                    <input class='form-control' type='text' name='address' id='address'/>

                </div>

                <div class='form-group'>
                    <label for='phone'>Telefono</label>
                    <input class='form-control' type='text' name='phone' id='phone'/>
                </div>

                <div class='form-group'>
                    <label for='quota'>Cuota</label>
                    <input class='form-control' type='text' name='quota' id='quota'/>
                </div>
                <div class='form-group'>
                    <label for='balance_quota'>Saldo cuota</label>
                    <input class='form-control' type='text' name='balance_quota' id='balance_quota'/>

                </div>
                <div class='form-group'>
                    <label for='percentage_visits' class="d-block">Porcentaje</label>
                   {{-- <input class='form-control' type='text' name='percentage_visits' id='percentage_visits'/>--}}
                    <div class="col-10 sliderContainer d-inline" style="width: 100%">
                        <input class="form-control" name="percentage_visits" type="text" />
                    </div>
                    <div class="col-2 d-inline">
                        <span class="percentageValue">5%</span>
                    </div>

                </div>
                <div class='form-group'>
                    <label for='country'>Pais</label>
                    {!! Form::select('country',[''=>'Selected..']+$countries,null,['class'=>'form-control','id'=>'country',
                    'data-url'=>route('departaments.city'),'data-get'=>route('countries.index')]); !!}
                </div>
                <div class='form-group'>
                    <label for='departament'>Departamento</label>
                    {!! Form::select('departament',[''=>'Selected..'],null,['class'=>'form-control','disabled'=>'true',
                    'id'=>'departament','data-url'=>route('cities.departament'),'data-get'=>route('departaments.index')]); !!}
                </div>
                <div class='form-group'>
                    <label for='city'>Cuidad</label>
                    {!! Form::select('city_id',[''=>'Selected..'],null,['class'=>'form-control','disabled'=>'true',
                    'id'=>'city','data-url'=>'','data-get'=>route('city.index')]); !!}
                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary customer insert" id="submit_button">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
