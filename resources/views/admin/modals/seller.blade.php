<!-- Modal -->
<div class="modal fade" id="modal_seller" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Vendedor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url'=>route('sellers.store'),'method'=>'post','class'=>'form_seller',
                'data-update'=>route('sellers.update',null),'data-store'=>route('sellers.store'),'data-id'=>'']) !!}
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input class="form-control" type="text" name="name" id="name">
                </div>
                <div class='form-group'>
                    <label for='email'>Email</label>
                    <input class='form-control' type='text' name='email' id='email'/>
                </div>
                <div class='form-group'>
                    <label for='address'>Direccion</label>
                    <input class='form-control' type='text' name='address' id='address'/>
                </div>
                <div class='form-group'>
                    <label for='type_card'>Tipo de tarjeta</label>
                    {!! Form::select('type_card',['cc'=>'C.C','ti'=>'T.I'],null,['class'=>'form-control','id'=>'type_card']); !!}
                </div>
                <div class='form-group'>
                    <label for='number_card'>Numero de tarjeta</label>
                    <input class='form-control' type='text' name='number_card' id='number_card'/>
                </div>
                <div class='form-group'>
                    <label for='phone_number'>Telefono</label>
                    <input class='form-control' type='text' name='phone_number' id='phone_number'/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary seller insert" id="submit_button">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
