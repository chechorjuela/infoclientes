@extends('layout.layout')
@section('content')
    <h1>Clientes</h1>
    <div class="row">
        <div class="col-12 text-right">
            <button class="btn pull-right btn-primary"  data-toggle="modal" data-target="#modal_customer"><i class="material-icons">Agregar</i></button>
        </div>
    </div>

    @if(count($customers)>0)
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Telefono</th>
                    <th>cupo</th>
                    <th>Saldo cupo</th>
                    <th>Porcentaje cup</th>
                    <th class="text-center">Geo</th>
                    <th class="text-center" colspan="3">Acción</th>
                </tr>
            </thead>
            <tbody class="body-customer" data-url="{!! route('customers.index') !!}" data-delete="{!! route('customers.destroy',null) !!}" data-show="{!! route('customers.show',null) !!}">
                @foreach($customers as $customer)
                    <tr>
                        <td>{!! $customer->id !!}</td>
                        <td>{!! $customer->name !!}</td>
                        <td>{!! $customer->address !!}</td>
                        <td>{!! $customer->phone !!}</td>
                        <td>{!! $customer->quota !!}</td>
                        <td>{!! $customer->balance_quota !!}</td>
                        <td>{!! $customer->percentage_visits !!}</td>
                        <td>{!! $customer->city->name !!}-{!! $customer->city->departament->name !!}</td>
                        <td><a href="{!! route('admin.reports.customers',$customer->id) !!}" data-id="{!! $customer->id !!}" class="btn btn-primary visit">Reporte</a></td>
                        <td><button data-id="{!! $customer->id !!}" class="btn btn-primary visit">Visita</button></td>
                        <td><button data-id="{!! $customer->id !!}" class="btn btn-primary edit">Editar</button></td>
                        <td><button data-id="{!! $customer->id !!}" class="btn btn-danger delete">Eliminar</button></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-12">
               {!!  $customers->render(); !!}
            </div>
        </div>
    @else
        <h2>No existe clientes</h2>
    @endif
    @include('admin.modals.customer')
    @include('admin.modals.visit')
@endsection
@section('scripts')
    {!! Html::script('js/admin.js') !!}
    {!! Html::script('js/customers.js'); !!}
    {!! Html::script('js/visits.js'); !!}
@endsection
