<?php

use Illuminate\Database\Seeder;

class DepartamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departaments')->insert([
            'name' => 'Antioquia',
            'country_id'=>1
        ]);
        DB::table('departaments')->insert([
            'name' => 'Cundinamarca',
            'country_id'=>1
        ]);
        DB::table('departaments')->insert([
            'name' => 'Tolima',
            'country_id'=>1
        ]);
    }
}
