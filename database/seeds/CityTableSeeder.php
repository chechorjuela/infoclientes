<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'name' => 'Medellin',
            'departament_id'=>1
        ]);
        DB::table('cities')->insert([
            'name' => 'Envigado',
            'departament_id'=>1
        ]);
        DB::table('cities')->insert([
            'name' => 'Rio negro',
            'departament_id'=>1
        ]);

        DB::table('cities')->insert([
            'name' => 'Bogota',
            'departament_id'=>2
        ]);
        DB::table('cities')->insert([
            'name' => 'Cota',
            'departament_id'=>2
        ]);
        DB::table('cities')->insert([
            'name' => 'Fusagazuga',
            'departament_id'=>2
        ]);

        DB::table('cities')->insert([
            'name' => 'Ibague',
            'departament_id'=>3
        ]);
        DB::table('cities')->insert([
            'name' => 'Espinal',
            'departament_id'=>3
        ]);
        DB::table('cities')->insert([
            'name' => 'Nuevo armero',
            'departament_id'=>3
        ]);
    }
}
